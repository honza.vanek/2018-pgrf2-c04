package rasterizationops;

import org.jetbrains.annotations.NotNull;
import rasterdata.RasterImage;


public interface LineRasterizerLerp<S, T> {
    /**
     * Rasterizes a line in normalized coordinates ([-1;1] square),
     * upper image left corner in [-1;1], lower left corner in [1;-1]
     * @param background image to "add" the line to
     * @param x1 x-coordinate of the start-point, in [-1;1]
     * @param y1 y-coordinate of the start-point, in [-1;1]
     * @param x2 x-coordinate of the end-point, in [-1;1]
     * @param y2 y-coordinate of the end-point, in [-1;1]
     * @param value1 value of the start-point
     * @param value2 value of the end-point
     * @return new image with the line added on the background
     */
    @NotNull RasterImage<T> rasterize(
            @NotNull RasterImage<T> background,
            double x1, double y1, double x2, double y2,
            @NotNull S value1, @NotNull S value2);
}
