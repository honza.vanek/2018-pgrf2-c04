import org.jetbrains.annotations.NotNull;
import transforms.Col;
import transforms.Point3D;
import transforms.Vec3D;

public class DepthPixel {
    private final @NotNull Col col;
    private final double depth;

    public DepthPixel(@NotNull Col col, double depth) {
        this.col = col;
        this.depth = depth;
    }

    public @NotNull Col getCol() {
        return col;
    }

    public double getDepth() {
        return depth;
    }

    public @NotNull DepthPixel mul(final double coef) {
        return new DepthPixel(
                col.mul(coef), depth * coef);
    }

    public @NotNull DepthPixel add(final @NotNull DepthPixel rhs) {
        return new DepthPixel(
                col.add(rhs.col), depth + rhs.depth);
    }
}
