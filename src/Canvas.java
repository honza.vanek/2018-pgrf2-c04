import io.vavr.Function3;
import org.jetbrains.annotations.NotNull;
import rasterdata.*;
import rasterizationops.*;
import solidops.RenderSolid;
import solidops.RenderWireframe;
import solids.Cube;
import solids.CubeSolid;
import solids.VertexColNorm;
import transforms.*;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.function.Function;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * trida pro kresleni na platno: zobrazeni pixelu
 *
 * @author PGRF FIM UHK
 * @version 2017
 */

/* TODO
   * X Objekty z trojuhelniku
   * X Rasterizer trojuhelniku s interp
   * RendererSolid (alespono Topo TRIANGLE_LIST, transform, orez, dehog)
   * X Obrazek s blendovanim
   * Obrazek s hloubkou
   		* trida ZPixel
   		* Tuple2<Col, Double>
   		* X splitter (rychlost)
   * Scena (model + controller)
 */

public class Canvas {

	private final JFrame frame;
	private final JPanel panel;
	private final BufferedImage img;

	private @NotNull RasterImage<Col> rasterImage;
	private final @NotNull ImagePresenter<Col, Graphics> presenter;

	private final @NotNull LineRasterizer<Col> lineRasterizer;
	private final @NotNull LineRasterizerLerp<Col, Col> lineRasterizerLerp;
	private final @NotNull TriangleRasterizer<Col, Col> triangleRasterizer;
	private int startC, startR;

	private final static Function3<Col, Col, Double, Col> COL_LERP =
			(v1, v2, s) -> v1.mul(1 - s).add(v2.mul(s));

	public Canvas(final int width, final int height) {
		frame = new JFrame();

		frame.setLayout(new BorderLayout());
		frame.setTitle("UHK FIM PGRF : " + this.getClass().getName());
		frame.setResizable(false);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
/*
		rasterImage = new RasterImageAWT<>(
			img,
			//Function<PixelType,Integer>, kde PixelType = Color
				(Color c) -> c.getRGB(),
			//Function<Integer,PixelType>, kde PixelType = Color
				(Integer i) -> new Color(i)
		);
		presenter = new ImagePresenterAWT<>();
/*/
		rasterImage = RasterImageImmu.cleared(
				width, height, new Col(255,0,0));
		presenter = new ImagePresenterUniversal<>(color -> color.getARGB());
//*/
		lineRasterizer = new LineRasterizerDDA<>();
		lineRasterizerLerp = new LineRasterizerDDALerp<>(
				COL_LERP,
//				(Col v1, Col v2, Double s) -> {
//					return v1.mul(1 - s).add(v2.mul(s));
//				},
				Function.identity()
		);

		triangleRasterizer = new TriangleRasterizerScan<>(
				COL_LERP,
				Function.identity()
		);

		panel = new JPanel() {
			private static final long serialVersionUID = 1L;

			@Override
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				present(g);
			}
		};

		panel.setPreferredSize(new Dimension(width, height));
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				startC = e.getX();
				startR = e.getY();
			}
		});
		panel.addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				final int endC = e.getX();
				final int endR = e.getY();
				final double x1 =
						2 * (startC + 0.5) / rasterImage.getWidth() - 1;
				final double y1 =
						-(2 * (startR + 0.5) / rasterImage.getHeight() - 1);
				final double x2 =
						2 * (endC + 0.5) / rasterImage.getWidth() - 1;
				final double y2 =
						-(2 * (endR + 0.5) / rasterImage.getHeight() - 1);
				rasterImage = lineRasterizerLerp.rasterize(
						rasterImage.cleared(new Col(0,0,0)),
						-1, 1, x2, y2,
						new Col(1f, 0, 1),
						new Col(0f, 1, 1)
				);
				rasterImage = triangleRasterizer.rasterize(rasterImage,
						0, 0, x1, y1, x2, y2,
						new Col(1f, 0, 0),
						new Col(0, 0, 1f),
						new Col(0f, 1, 0)
						);
				panel.repaint();
			}
		});

		frame.add(panel, BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);
	}

	public void clear() {
		rasterImage = rasterImage.cleared(new Col(0x2f, 0x2f, 0x2f));
	}

	public void present(final Graphics graphics) {
		presenter.present(rasterImage, graphics);
	}

	public void draw() {
		clear();
		rasterImage = rasterImage.withPixel(10,10,
				new Col(0xffff00));
		/*
		rasterImage = lineRasterizer.rasterize(
				rasterImage,
				-0.8, -0.5, 0.8, 0.5,
				new Color(1.0f, 0, 1)
		);
		rasterImage = lineRasterizer.rasterize(
				rasterImage,
				-0.8, 0.5, 0.8, -0.5,
				new Color(1.0f, 0, 1)
		);
		rasterImage = lineRasterizer.rasterize(
				rasterImage,
				-0.8, -0.8, 0.8, -0.8,
				new Color(1.0f, 0, 1)
		);
		rasterImage = new SeedFill4<Color>().fill(rasterImage,
				rasterImage.getWidth() / 2, rasterImage.getHeight() / 2 + 5,
				new Color(0x00ffff),
				pixel -> pixel.equals(new Color(0x2f, 0x2f, 0x2f)));
		*/

//		final Camera cam = new Camera()
//				.withPosition(new Vec3D(5,3,2))
//				.withAzimuth(Math.PI)
//				.withZenith(-Math.atan(2.0 / 5.0));
		final Camera cam = new Camera()
				.withPosition(new Vec3D(1.1,-0.1,1.1))
				.withAzimuth(0.75 * Math.PI)
				.withZenith(-Math.atan(3.0 / 5.0));
		final Mat4 persp = new Mat4PerspRH(
				Math.PI / 3,
				rasterImage.getHeight() / (double) rasterImage.getWidth(),
				0.2, 1000
		);


		rasterImage =
			new RenderSolid<Col, Point3D, Col>(
				lineRasterizerLerp,
				triangleRasterizer,
				Function.identity(),
				(Point3D vertex, Double depth) -> new Col(vertex),
				(v1, v2, t) -> v1.mul(1 - t).add(v2.mul(t))
			).render(rasterImage, new Cube(),
					new Mat4Transl(0,2, 0)
							.mul(cam.getViewMatrix())
							.mul(persp),
					new Col(1.0f, 1, 0));
		rasterImage =
			new RenderSolid<Col, VertexColNorm, Col>(
				lineRasterizerLerp,
				new TriangleRasterizerScan<>(
						COL_LERP,
						Function.identity()
				),
				(VertexColNorm vertex) -> vertex.getPos(),
				(VertexColNorm vertex, Double depth) -> vertex.getCol(),
				(v1, v2, t) -> v1.mul(1 - t).add(v2.mul(t))
			).render(rasterImage, new CubeSolid(),
					new Mat4Transl(0,0, 0)
						.mul(cam.getViewMatrix())
						.mul(persp),
					new Col(1.0f, 1, 0));
	}

	public void start() {
		draw();
		panel.repaint();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Canvas(800, 600)::start);
	}

}