package rasterdata;

import org.jetbrains.annotations.NotNull;

public interface ImagePresenter<SomePixelType, DeviceType> {
    @NotNull DeviceType present(
            @NotNull RasterImage<SomePixelType> image,
            @NotNull DeviceType device);
}
