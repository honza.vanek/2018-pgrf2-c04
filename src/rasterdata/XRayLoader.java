package rasterdata;

import io.vavr.Function4;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.Seq;
import io.vavr.collection.Stream;
import org.jetbrains.annotations.NotNull;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

public class XRayLoader {
    /**
     * Loads an x-ray image from an {@link InputStream}. The image must be stored in 16-bit unsigned little-endian integer format. Pixels are normalized to [0;1].
     * @param width result image width
     * @param height result image height
     * @param dataWidth original image width
     * @param dataHeight original image height
     * @param inputStream the {@link InputStream} to load from
     * @param empty an empty result image
     * @param withPixel a method that returns an image with a pixel at the given column-row address set to the given value
     * @param <T> the type of the result image
     * @return an image loaded from the {@link InputStream}
     */
    public static @NotNull
    <T> T loadXRayShort(final int width, final int height,
                        final int dataWidth, final int dataHeight,
                        final @NotNull InputStream inputStream,
                        final @NotNull T empty,
                        final @NotNull Function4<T, Integer, Integer, Double, T> withPixel) {
        final @NotNull Seq<Integer> data;
        try {
            final @NotNull DataInputStream dataInputStream = new DataInputStream(inputStream);
            data = Stream.range(0, dataHeight * dataWidth).map(
                    i -> {
                        try {
                            return dataInputStream.readUnsignedShort();
                        } catch (Exception e) {
                            e.printStackTrace();
                            return 0;
                        }
                    }
            ).toArray();
            dataInputStream.close();
        } catch(Exception e) {
            e.printStackTrace();
            return empty;
        }
        final @NotNull Tuple2<Integer, Integer> minMax = data.foldLeft(Tuple.of(65535, 0),
                (currentMinMax, val) ->  Tuple.of(
                        val < currentMinMax._1 ? val : currentMinMax._1,
                        val > currentMinMax._2 ? val : currentMinMax._2
                )
        );
        return Stream.range(0, height).foldLeft(empty,
                (rowImage, row) -> Stream.range(0, width).foldLeft(rowImage,
                        (colImage, column) -> {
                            final int dataRow = (dataHeight - 1) * row / (height - 1);
                            final int dataCol = (dataWidth - 1) * column / (width - 1);
                            final double xRay = (double) (data.get(dataRow * dataWidth + dataCol) - minMax._1) / (minMax._2 - minMax._1);
                            return withPixel.apply(colImage, column, row, xRay);
                        }
                )
        );
    }

    /**
     * Loads an x-ray image from a file. The image must be stored in 16-bit unsigned little-endian integer format. Pixels are normalized to [0;1].
     * @param width result image width
     * @param height result image height
     * @param dataWidth original image width
     * @param dataHeight original image height
     * @param fileName name of the file to load from
     * @param empty an empty result image
     * @param withPixel a method that returns an image with a pixel at the given column-row address set to the given value
     * @param <T> the type of the result image
     * @return an image loaded from the file
     */
    public static @NotNull
    <T> T loadXRayShort(final int width, final int height,
                        final int dataWidth, final int dataHeight,
                        final @NotNull String fileName,
                        final @NotNull T empty,
                        final @NotNull Function4<T, Integer, Integer, Double, T> withPixel) {
        try {
            final @NotNull InputStream inputStream = new FileInputStream(fileName);
            T result = loadXRayShort(width, height, dataWidth, dataHeight, inputStream, empty, withPixel);
            inputStream.close();
            return result;
        } catch(Exception e) {
            e.printStackTrace();
            return empty;
        }
    }

}
