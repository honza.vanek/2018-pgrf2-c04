package rasterdata;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;
import java.util.function.Function;

public class RasterImageSplitter<T, U, V> implements RasterImage<T> {
    private final @NotNull RasterImage<U> imageU;
    private final @NotNull RasterImage<V> imageV;
    private final @NotNull Function<T, Tuple2<U, V>> fromT;
    private final @NotNull Function<Tuple2<U, V>, T> toT;

    public RasterImageSplitter(@NotNull RasterImage<U> imageU, @NotNull RasterImage<V> imageV, @NotNull Function<T, Tuple2<U, V>> fromT, @NotNull Function<Tuple2<U, V>, T> toT) {
        this.imageU = imageU;
        this.imageV = imageV;
        this.fromT = fromT;
        this.toT = toT;
    }

    @NotNull
    @Override
    public Optional<T> getPixel(int c, int r) {
        return imageU.getPixel(c, r).flatMap(
                pixelU -> imageV.getPixel(c, r).map(
                        pixelV -> toT.apply(Tuple.of(pixelU, pixelV))
                )
        );
    }

    @NotNull
    @Override
    public RasterImage<T> withPixel(int c, int r, @NotNull T value) {
        return fromT.apply(value).apply(
            (pixelU, pixelV) -> new RasterImageSplitter<>(
                    imageU.withPixel(c, r, pixelU),
                    imageV.withPixel(c, r, pixelV),
                    fromT, toT)
        );
    }

    @NotNull
    @Override
    public RasterImage<T> cleared(@NotNull T value) {
        return fromT.apply(value).apply(
                (pixelU, pixelV) -> new RasterImageSplitter<>(
                        imageU.cleared(pixelU),
                        imageV.cleared(pixelV),
                        fromT, toT)
        );
    }

    @Override
    public int getWidth() {
        return Math.min(imageU.getWidth(), imageV.getWidth());
    }

    @Override
    public int getHeight() {
        return Math.min(imageU.getHeight(), imageV.getHeight());
    }
}
